# NopCommerce Product Extension

This is a nopCommerce plugin that demonstrates how it is possible to extend an existing entity in the admin ui. Please read this blog article for more information: https://viu.ch/blog/2020/nopcommerce-extend-admin-interface.html

This code is for demonstration purposes only and should not be used in production environments as is.

## Getting Started

### Prerequisites

The nopCommerce package with source code is required to run this plugin. Please download it here:

```
https://www.nopcommerce.com/download-nopcommerce
```

This plugin is only responsible for the admin user interface which means the data layer has to be implementend prior to deploying this code. You will find the "IExtendedProductService" interface in the "Interfaces to implement" folder which you need to implement yourself. If you are unsure on how to do that, please follow this link: https://docs.nopcommerce.com/developer/plugins/plugin-with-data-access.html

### Installing

1. Clone this repo into the plugin folder of your nopCommerce instance and add it to the solution as an existing project. 
2. Make sure you have implemented the data layer to be able to build the project.
3. Build and run the project.
4. Go to "Local Plugins" in the backend and install this plugin
5. Since it is a Widget Plugin, make sure it is enabled.
6. Check out /Admin/Product/Edit/#anyProductId to see the new tab

You should now be able to add or remove existing blog posts which persist after a reload (i.e. it actually stores the relationship in the database).

### What's next

Storing a relationship such as the one described above doesn't do much on its own. In a next step you should implement some kind of front end facing widget that makes use of the stored data. This code and the aforementioned blog article are based on a real use case. 

An example of the implementation can be found here: https://www.betriebsapotheke.ch/druckverband-zivil

You can see the blue area below the product that lists related blog posts.

## Contributing

Feel free to suggest changes or optimizations via a merge request.

## Authors

* **Rolf Isler** - *Initial work* - [VIU AG](https://viu.ch)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Thanks to [VIU AG](https://viu.ch), my employer, for letting me publish this code.
* [NopCommerce](https://nopcommerce.com) for being an awesome commerce platform



