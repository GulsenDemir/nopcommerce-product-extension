﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Framework.Components;
using VIU.Plugin.Extensions.Product.Admin.Model;
using VIU.Plugin.Extensions.Product.Admin.Model.RelatedBlogPost;

namespace VIU.Plugin.Extensions.Product.Admin.Components
{
    [ViewComponent(Name = "ProductExtension")]
    public class ProductExtensionComponent : NopViewComponent
    {
        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            if (!(additionalData is ProductModel productModel)) return Content("");
            
            var model = new ExtendedProductModel{
                ProductId = productModel.Id,
                RelatedBlogPostSearchModel = new RelatedBlogPostSearchModel{
                    ProductId = productModel.Id
                }
            };
            
            return View("~/Plugins/VIU.Extensions.Product.Admin/Views/Default.cshtml", model);
        }
    }
}