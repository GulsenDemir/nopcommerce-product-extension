﻿using System.Collections.Generic;
using Nop.Core;
using VIU.Plugin.Extensions.Product.Core.Domain;

namespace VIU.Plugin.Extensions.Product.Core.Services
{
    public interface IExtendedProductService
    {
        #region RelatedBlogPost

        void DeleteRelatedBlogPost(RelatedBlogPost relatedBlogPost);

        IPagedList<RelatedBlogPost> GetRelatedBlogPostsByProductId(int productId,
            int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false);

        RelatedBlogPost GetRelatedBlogPostById(int relatedBlogPostId);

        void InsertRelatedBlogPost(RelatedBlogPost relatedBlogPost);

        void UpdateRelatedBlogPost(RelatedBlogPost relatedBlogPost);

        #endregion
    }
}
