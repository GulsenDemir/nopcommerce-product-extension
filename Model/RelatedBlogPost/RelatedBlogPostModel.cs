﻿using Nop.Web.Framework.Models;

namespace VIU.Plugin.Extensions.Product.Admin.Model.RelatedBlogPost
{
    public class RelatedBlogPostModel : BaseNopEntityModel
    {
        public int RelatedBlogPostId { get; set; }
        public string Title { get; set; }
        public string LanguageName { get; set; }
        public int DisplayOrder { get; set; }
    }
    
    public class RelatedBlogPostSearchModel : BaseSearchModel
    {
        public int ProductId { get; set; }
    }
    
    public class RelatedBlogPostListModel : BasePagedListModel<RelatedBlogPostModel> { }
}