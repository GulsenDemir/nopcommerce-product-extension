﻿using VIU.Plugin.Extensions.Product.Admin.Model.RelatedBlogPost;

namespace VIU.Plugin.Extensions.Product.Admin.Model
{
    public class ExtendedProductModel
    {
        public ExtendedProductModel()
        {
            RelatedBlogPostSearchModel = new RelatedBlogPostSearchModel();
        }

        public int ProductId { get; set; }
        
        public RelatedBlogPostSearchModel RelatedBlogPostSearchModel { get; set; }
    }
}