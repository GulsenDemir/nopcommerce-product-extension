﻿using System.Collections.Generic;
using Nop.Services.Cms;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;

namespace VIU.Plugin.Extensions.Product.Admin
{
    public class ProductAdminPlugin : BasePlugin, IWidgetPlugin
    {
        public bool HideInWidgetList => false;
        
        public IList<string> GetWidgetZones()
        {
            return new List<string>
            {
                AdminWidgetZones.ProductDetailsBlock
            };
        }

        public string GetWidgetViewComponentName(string widgetZone)
        {
            if (widgetZone.Equals(AdminWidgetZones.ProductDetailsBlock))
                return "ProductExtension";
            
            return string.Empty;
        }
    }
}